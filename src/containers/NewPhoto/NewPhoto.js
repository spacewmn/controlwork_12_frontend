import React, {useState} from 'react';
import FormElement from "../../components/FormElement/FormElement";
import {useDispatch} from "react-redux";
import FileInput from "../../components/FileInput/FileInput";
import {createNewPhoto} from "../../store/actions/photoActions";

const NewPhoto = (props) => {
    const dispatch = useDispatch();
    const [state, setState] = useState({
        title: "",
        image: ""
    });


    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const formSubmitHandler = e => {
        e.preventDefault();
        let data = new FormData();
        Object.keys(state).forEach(key => {
            data.append(key, state[key]);
        });
        dispatch(createNewPhoto(data)).then(() => {
            props.history.push("/")})
    };

    return (
        <form className="mb-5" onSubmit={formSubmitHandler}>
            <FormElement
                name='title'
                onChange={inputChangeHandler}
                label='Title'
                type='text'
                value={state.title}
            />
            <FileInput
                name="image"
                onChange={fileChangeHandler}
            />
            <button type="submit" className="btn btn-primary">Add photo</button>
        </form>
    );
};

export default NewPhoto;