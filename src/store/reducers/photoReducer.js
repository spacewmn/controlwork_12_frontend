import {DELETE_PHOTO_SUCCESS, FETCH_PHOTO_SUCCESS, FETCH_PHOTOS_SUCCESS} from "../actionTypes";

const initialState = {
    photos: [],
    // myCocktails: [],
    photo: '',
};


const cocktailsReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_PHOTOS_SUCCESS:
            return {...state, photos: action.photos};
        // case FETCH_MY_COCKTAILS_SUCCESS:
        //     return {...state, myCocktails: action.cocktails};
        case FETCH_PHOTO_SUCCESS:
            return {...state, photo: action.photo};
        case DELETE_PHOTO_SUCCESS:
            const newGallery = [...state.photos];
            const id = newGallery.findIndex(p => p.id === action.id);
            newGallery.splice(id, 1);
            return {...state, photos : [...newGallery]};
        default:
            return state;
    }

};

export default cocktailsReducer;