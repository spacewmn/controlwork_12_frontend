import axios from "../../axiosApi";
import {push} from "connected-react-router";
import {
    CREATE_PHOTO_ERROR,
    CREATE_PHOTO_SUCCESS,
    DELETE_PHOTO_SUCCESS,
    FETCH_PHOTO_SUCCESS,
    FETCH_PHOTOS_SUCCESS, FETCH_USER_PHOTOS_SUCCESS
} from "../actionTypes";

const createNewPhotoError = (error) => {
    return {type: CREATE_PHOTO_ERROR, error}
};

export const createNewPhoto = data => {
    return async dispatch => {
        try {
            console.log(data, 'data');

            await axios.post('/gallery', data);
            dispatch({type: CREATE_PHOTO_SUCCESS});
            dispatch(push("/"));
        } catch (e) {
            dispatch(createNewPhotoError(e.response.data.error));
        }
    };
};

const fetchPhotosSuccess = photos => {
    return {type: FETCH_PHOTOS_SUCCESS, photos};
};

export const fetchPhotos = () => {
    return async dispatch => {
        try {
            // await axios.get("/gallery").then(response => {
            //     dispatch(fetchPhotosSuccess(response.data));
            // });
            const response = await axios.get("/gallery")
            dispatch(fetchPhotosSuccess(response.data));
        } catch (e) {
            console.log(e);
        }
    };
};


const fetchPhotoSuccess = photo => {
    return {type: FETCH_PHOTO_SUCCESS, photo};
};

export const fetchPhoto = (id) => {
    return async dispatch => {
        try {
            await axios.get("/gallery/" + id).then(response => {
                dispatch(fetchPhotoSuccess(response.data));
            });
        } catch (e) {
            console.log(e);
        }
    };
};

const deletePhotoSuccess = (id) => {
    return {type: DELETE_PHOTO_SUCCESS, id}
};

export const deletePhoto = id => {
    return async dispatch => {
        try {
            const response = await axios.delete('/gallery/' + id);
            dispatch(deletePhotoSuccess(id));
        } catch (e) {
            console.log(e);
        }
    };
};

const fetchUserPhotosSuccess = photos => {
    return {type: FETCH_USER_PHOTOS_SUCCESS, photos};
};

export const fetchUserPhotos = (id) => {
    return  async dispatch => {
        try {
            await axios.get("/gallery?user=" + id).then(response => {
                dispatch(fetchUserPhotosSuccess(response.data));
            });
        } catch (e) {
            console.log(e);
        }
    };
};
