import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import NewPhoto from "./containers/NewPhoto/NewPhoto";
import Gallery from "./components/Gallery/Gallery";
import UserPhoto from "./components/UserPhoto/UserPhoto";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />
};

const Routes = ({user}) => {
    return (
        <Switch>
            <ProtectedRoute
                path='/'
                exact
                component={Gallery}
                isAllowed={!user || user}
                redirectTo="/"
            />
            <ProtectedRoute
                path='/users/:id'
                exact
                component={UserPhoto}
                isAllowed={!user || user}
            />
            <ProtectedRoute
                path='/new_photo'
                exact
                component={NewPhoto}
                isAllowed={user}
                redirectTo="/login"
            />
            <ProtectedRoute
                path='/register'
                exact
                component={Register}
                isAllowed={!user}
                redirectTo="/"
            />
            <ProtectedRoute
                path='/login'
                exact
                component={Login}
                isAllowed={!user}
                redirectTo="/"
            />
        </Switch>
    );
};

export default Routes;