import React from 'react';
import {Link} from "react-router-dom";
import {logoutUser} from "../../store/actions/userActions";
import {useDispatch, useSelector} from "react-redux";
import defaultAvatar from "../../assets/images/default.jpg";


const myStyle = {
    width: "50px",
    height:"50px"
};

const UserMenu = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    const logout = () => {
        dispatch(logoutUser());
    };

    return (
        <>
            {/*<div className="font-weight-bold text-light ">Hello, {user.user.user.displayName}!*/}
            {/*    <img src={user.user.user.avatar ? user.user.user.avatar : defaultAvatar }*/}
            {/*         className="rounded circle rounded-circle m-3"*/}
            {/*         style={myStyle}*/}
            {/*         alt="user"*/}
            {/*    />*/}
            {/*</div>*/}
            <div>
                <button type="button" className="btn btn-outline-warning" onClick={logout}>Logout</button>
            </div>
            {
                user  && <div className="d-block">
                    <Link className="nav-link" to="/new_photo">Add photo</Link>
                    {/*<Link className="nav-link" to="/add_album">Add album</Link>*/}
                </div>
            }
        </>
    );
};

export default UserMenu;