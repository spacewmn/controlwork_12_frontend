import React from 'react';
import {NavLink} from "react-router-dom";
import imageNotAvailable from '../../assets/images/default.jpg';
import {apiURL} from '../../constants';
import Modal from "../Modal/Modal";
import useModal from '../Modal/useModal';
import {deletePhoto} from '../../store/actions/photoActions';
import {useDispatch} from "react-redux";

const PhotoItem = (props, userId, id) => {
    const dispatch = useDispatch();
    let cardImage = imageNotAvailable;
    // const {isShowing, toggle} = useModal();

    if (props.image) {
        cardImage = apiURL + '/uploads/' + props.image;
    }

    const removePhoto = (id) => {
        dispatch(deletePhoto(id));
    };

    return (
        <div className="card m-3" style={{"width": "18rem"}} id={props.id}>
            <button
                // onClick={toggle}
            >
                <img src={cardImage} className="card-img-top" alt="..."/>
            </button>
            {/*<Modal*/}
            {/*    isShowing={isShowing}*/}
            {/*    hide={toggle}*/}
            {/*/>*/}
                <div className="card-body">
                    <h5 className="card-title">{props.title}</h5>
                    {props.userId && props.user &&
                        <NavLink to={"/users/" + props.userId} className="btn btn-primary">By {props.user}</NavLink>
                    }
                    {userId === id ?
                        <button type="button" className="btn btn-danger" onClick={()=>removePhoto(props.id)}>Delete</button>
                        : null}

                </div>
        </div>
    );
};

export default PhotoItem;