import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchUserPhotos} from "../../store/actions/photoActions";
import PhotoItem from "../PhotoItem/PhotoItem";
import {NavLink} from "react-router-dom";


const UserPhoto = (props) => {

    const user = useSelector(state => state.users.user);
    const userPhotos = useSelector(state => state.photos.photos);
    const dispatch = useDispatch();
    const id = user._id;
    const userId = props.match.params.id


    useEffect(() => {
        dispatch(fetchUserPhotos(userId));
    }, [dispatch,userId]);


    return (

        <div className="container">
            {userId === id ?
                <NavLink className="btn btn-primary font-weight-bold  m-3" to={"/new_photo"}>Add photo</NavLink>
                : null}
            <h1>{user.username}'s photos</h1>
            <div className="row">
                {userPhotos.map(photo => {
                    return <PhotoItem
                        key={photo._id}
                        id={photo._id}
                        title={photo.title}
                        image={photo.image}
                        userId
                        id
                    />
                })}
            </div>
        </div>

    );
};

export default UserPhoto;