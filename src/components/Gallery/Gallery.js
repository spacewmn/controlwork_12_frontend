import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotos} from "../../store/actions/photoActions";
import PhotoItem from "../PhotoItem/PhotoItem";

const Gallery = () => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);

    useEffect(() => {
        // console.log(photos, 'us');
        dispatch(fetchPhotos());
    }, [dispatch]);


    return photos && (
        <div className="container">
            <div className="row">
                {photos.map(photo => {
                    return <PhotoItem
                        key={photo._id}
                        id={photo._id}
                        userId={photo.user._id}
                        title={photo.title}
                        image={photo.image}
                        user={photo.user.username}
                    />
                })}
            </div>
        </div>
    );
};

export default Gallery;